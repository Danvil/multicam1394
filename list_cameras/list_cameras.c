

#include <stdio.h>
#include <stdint.h>
#include <dc1394/dc1394.h>
#include <stdlib.h>
#include <inttypes.h>

#ifndef _WIN32
#include <unistd.h>
#endif

int main(int argc, char *argv[])
{
    FILE* imagefile;
    dc1394_t* d;
    dc1394camera_list_t* list;
    dc1394error_t err;

    d = dc1394_new();
    if(!d)
        return 1;
    err = dc1394_camera_enumerate(d, &list);
    DC1394_ERR_RTN(err, "Failed to enumerate cameras");

    if (list->num == 0) {
        dc1394_log_error("No cameras found");
        return 1;
    }

	if(list->num > 0) {
		uint32_t i;
		printf("Found %d camera(s): \n", list->num);
		printf("Index\tGUID\n");
		printf("-------------------\n");
		for(i=0; i<list->num; i++) {
		    printf("%d\t%"PRIx64"\n", i, list->ids[i].guid);
		}
	} else {
		printf("Found no cameras!");
	}

    dc1394_free (d);
    return 0;
}

