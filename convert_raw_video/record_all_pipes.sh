#!/bin/sh

VIDEOCOUNT=5
PIPEPREFIX=/tmp/vidpipe
OUTPUTPREFIX=raw
IMG_W=780
IMG_H=582
BITRATE=8000

for ((i = 0; i < $VIDEOCOUNT; i++))
do
  mencoder -demuxer rawvideo -rawvideo w=${IMG_W}:h=${IMG_H}:y8 -ovc lavc -lavcopts \
  vcodec=mpeg4:mbd=2:trell:v4mv:vbitrate=${BITRATE} ${PIPEPREFIX}${i} \
  -o ${OUTPUTPREFIX}${i}.avi &
done

