#!/bin/sh

VIDEOCOUNT=5
DIRPREFIX=camsmall
INPUTPREFIX=raw
FRAMEPREFIX=frame
ORI_W=780
ORI_H=582
NEW_W=384
NEW_H=288

for ((i = 0; i < $VIDEOCOUNT; i++))
do
  mkdir ${DIRPREFIX}${i}
  cd ${DIRPREFIX}${i}
  ../convert_raw_video ../${INPUTPREFIX}${i}.avi downsample jpg:${FRAMEPREFIX} crop:1:2:3:3 time
  cd ..
done

