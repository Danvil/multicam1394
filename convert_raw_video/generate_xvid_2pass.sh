#!/bin/bash

VIDEOCOUNT=3
INPUTPREFIX=raw
OUTPUTPREFIX=cam
ORI_W=640
ORI_H=480
NEW_W=640
NEW_H=480
FPS=60
BITRATE=8000

for ((i = 0; i < $VIDEOCOUNT; i++))
do
  ias_convert_raw_video ${INPUTPREFIX}${i}.avi edgesense stdout | mencoder - \
  -demuxer rawvideo -rawvideo w=${NEW_W}:h=${NEW_H}:fps=${FPS}:format=bgr24 -ovc xvid \
  -xvidencopts chroma_opt:vhq=4:bvhq=1:quant_type=mpeg:bitrate=${BITRATE}:pass=1 -o \
  ${OUTPUTPREFIX}${i}.avi
  
  ias_convert_raw_video ${INPUTPREFIX}${i}.avi edgesense stdout | mencoder - \
  -demuxer rawvideo -rawvideo w=${NEW_W}:h=${NEW_H}:fps=${FPS}:format=bgr24 -ovc xvid \
  -xvidencopts chroma_opt:vhq=4:bvhq=1:quant_type=mpeg:bitrate=${BITRATE}:pass=2 -o \
  ${OUTPUTPREFIX}${i}.avi
  
  rm divx2pass.log
done

