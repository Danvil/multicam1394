#!/bin/sh

VIDEOCOUNT=5
INPUTPREFIX=raw
OUTPUTPREFIX=cam
ORI_W=780
ORI_H=582
NEW_W=768
NEW_H=576
FPS=25

for ((i = 0; i < $VIDEOCOUNT; i++))
do
  ./convert_raw_video ${INPUTPREFIX}${i}.avi edgesense stdout crop:3:3:6:6 | mencoder - \
  -demuxer rawvideo -rawvideo w=${NEW_W}:h=${NEW_H}:fps=${FPS}:format=bgr24 -ovc lavc \
  -lavcopts vcodec=mjpeg -o ${OUTPUTPREFIX}${i}.avi
done

