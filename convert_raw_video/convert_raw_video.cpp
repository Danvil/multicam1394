/**
 * \file convert_raw_video.cpp
 * \author Jan Bandouch
 * \date 2007
 * \note Lehrstuhl fuer Informatik IX 
 *       Bildverstehen und Wissensbasierte Systeme
 *       Technische Universitaet Muenchen
 * \note Expects all original images sizes to be 32bit row aligned!
 */
 
#include <string>
#include <vector>
#include <iostream>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "sys/time.h"

using namespace std;

//#define IS_BIG_ENDIAN

#define CLIP(in, out)\
   in = in < 0 ? 0 : in;\
   in = in > 255 ? 255 : in;\
   out=in;

// ************************************************************************** //
// Debayering method taken from Coriander. We expect RGGB pattern. Low quality.
// ************************************************************************** //

void BayerNearestNeighbor(IplImage *src, IplImage *dst)
{
  int sx = src->width;
  int sy = src->height;
  register int i,j;

  // sx and sy should be even
  unsigned char *outB = (unsigned char*)&dst->imageData[2];
  unsigned char *outG = (unsigned char*)&dst->imageData[1];
  unsigned char *outR = (unsigned char*)&dst->imageData[0];
  unsigned char *srcdata = (unsigned char*)src->imageData;
  // copy original data
  for (i=0;i<sy;i+=2)
  {
    for (j=0;j<sx;j+=2)
    {
      outB[(i*sx+j)*3]=srcdata[i*sx+j];
      outR[((i+1)*sx+(j+1))*3]=srcdata[(i+1)*sx+(j+1)];
      outG[(i*sx+j+1)*3]=srcdata[i*sx+j+1];
      outG[((i+1)*sx+j)*3]=srcdata[(i+1)*sx+j];
    }
  }
  // R channel
  for (i=0;i<sy;i+=2)
  {
    for (j=0;j<sx-1;j+=2)
    {
      outR[(i*sx+j)*3]=outR[((i+1)*sx+j+1)*3];
      outR[(i*sx+j+1)*3]=outR[((i+1)*sx+j+1)*3];
      outR[((i+1)*sx+j)*3]=outR[((i+1)*sx+j+1)*3];
    }
  }
  // B channel
  for (i=0;i<sy-1;i+=2) { //every two lines
    for (j=0;j<sx-1;j+=2) {
      outB[((i+1)*sx+j)*3]=outB[(i*sx+j)*3];
      outB[(i*sx+j+1)*3]=outB[(i*sx+j)*3];
      outB[((i+1)*sx+j+1)*3]=outB[(i*sx+j)*3];
    }
  }
  // using lower direction for G channel
  
  // G channel
  for (i=0;i<sy-1;i+=2)//every two lines
    for (j=0;j<sx-1;j+=2)
      outG[(i*sx+j)*3]=outG[((i+1)*sx+j)*3];
  
  for (i=1;i<sy-2;i+=2)//every two lines
    for (j=0;j<sx-1;j+=2)
      outG[(i*sx+j+1)*3]=outG[((i+1)*sx+j+1)*3];
  
  // copy it for the next line
  for (j=0;j<sx-1;j+=2)
    outG[((sy-1)*sx+j+1)*3]=outG[((sy-2)*sx+j+1)*3];
}

// ************************************************************************** //
// Debayering method taken from Coriander. We expect RGGB pattern. Med quality.
// ************************************************************************** //

void BayerSimple(IplImage *src, IplImage *dst)
{
  int sx = src->width;
  int sy = src->height;
  register int i,j;
  int base;
  
  unsigned char* srcdata = (unsigned char*)src->imageData;
  unsigned char* dstdata = (unsigned char*)dst->imageData;
  unsigned char* outB=&dstdata[2];
  unsigned char* outG=&dstdata[1];
  unsigned char* outR=&dstdata[0];

  for (i=0;i<sy-1;i+=2)
  {
    for (j=0;j<sx-1;j+=2)
    {
      base=i*sx+j;
      outG[base*3] = ((srcdata[base+sx]+srcdata[base+1])>>1);
      outR[base*3] = srcdata[base+sx+1];
      outB[base*3] = srcdata[base];
    }
  }
  for (i=1;i<sy-1;i+=2)
  {
    for (j=0;j<sx-1;j+=2)
    {
      base=i*sx+j;
      outG[(base)*3] = ((srcdata[base]+srcdata[base+1+sx])>>1);
      outR[(base)*3] = srcdata[base+1];
      outB[(base)*3] = srcdata[base+sx];
    }
  }
  for (i=0;i<sy-1;i+=2)
  {
    for (j=1;j<sx-1;j+=2)
    {
      base=i*sx+j;
      outG[base*3] = ((srcdata[base]+srcdata[base+sx+1])>>1);
      outR[base*3] = srcdata[base+sx];
      outB[base*3] = srcdata[base+1];
    }
  }
  for (i=1;i<sy-1;i+=2)
  {
    for (j=1;j<sx-1;j+=2)
    {
      base=i*sx+j;
      outG[(base)*3] = ((srcdata[base+1]+srcdata[base+sx])>>1);
      outR[(base)*3] = srcdata[base];
      outB[(base)*3] = srcdata[base+1+sx];
    }
  }
  
  // copy border pixels
  int ws = dst->widthStep;
  int pos = ws - 3;
  for (i=0; i<sy; i++, pos+=ws)
  {
    dstdata[pos] = dstdata[pos-3];
    dstdata[pos+1] = dstdata[pos-2];
    dstdata[pos+2] = dstdata[pos-1];
  }
  for (i=ws*(sy-1); i<dst->imageSize; i++)
  {
    dstdata[i] = dstdata[i-ws];
    i++;
    dstdata[i] = dstdata[i-ws];
    i++;
    dstdata[i] = dstdata[i-ws];
  }
}

// ************************************************************************** //
// Debayering method taken from Coriander. We expect RGGB pattern. Optimized!
// ************************************************************************** //

void BayerEdgeSense(IplImage *src, IplImage *dst)
{
  int sx = src->width;
  int sy = src->height;
  register int i,j;
  int dh, dv;
  int tmp;
  int ws = dst->widthStep;

  // sx and sy should be even
  // RGGB pattern
  unsigned char *srcdata = (unsigned char*)src->imageData;
  unsigned char *dstdata = (unsigned char*)dst->imageData;
  unsigned char *outB = &dstdata[2];
  unsigned char *outG = &dstdata[1];
  unsigned char *outR = &dstdata[0];
  
  // copy original RGB data to output images
  for (i=0;i<sy;i+=2) 
  {
    for (j=0;j<sx;j+=2)
    {
      outB[(i*sx+j)*3]=srcdata[i*sx+j];
      outR[((i+1)*sx+(j+1))*3]=srcdata[(i+1)*sx+(j+1)];
      outG[(i*sx+j+1)*3]=srcdata[i*sx+j+1];
      outG[((i+1)*sx+j)*3]=srcdata[(i+1)*sx+j];
    }
  }
  // process GREEN channel
  for (i=2;i<sy-2;i+=2)
  {
    for (j=2;j<sx-3;j+=2)
    {
      dh=abs(((outB[(i*sx+j-2)*3]+outB[(i*sx+j+2)*3])>>1)-outB[(i*sx+j)*3]);
      dv=abs(((outB[((i-2)*sx+j)*3]+outB[((i+2)*sx+j)*3])>>1)-outB[(i*sx+j)*3]);
      if (dh<dv)
        tmp=(outG[(i*sx+j-1)*3]+outG[(i*sx+j+1)*3])>>1;
      else
      {
        if (dh>dv)
          tmp=(outG[((i-1)*sx+j)*3]+outG[((i+1)*sx+j)*3])>>1;
        else
          tmp=(outG[(i*sx+j-1)*3]+outG[(i*sx+j+1)*3]+outG[((i-1)*sx+j)*3]+outG[((i+1)*sx+j)*3])>>2;
      }
      outG[(i*sx+j)*3] = tmp;
    }
  }
  for (i=3;i<sy-2;i+=2) // JAN: Changed sy-3 to sy-2 !!!
  {
    for (j=3;j<sx-2;j+=2)
    {
      dh=abs(((outR[(i*sx+j-2)*3]+outR[(i*sx+j+2)*3])>>1)-outR[(i*sx+j)*3]);
      dv=abs(((outR[((i-2)*sx+j)*3]+outR[((i+2)*sx+j)*3])>>1)-outR[(i*sx+j)*3]);
      if (dh<dv)
        tmp=(outG[(i*sx+j-1)*3]+outG[(i*sx+j+1)*3])>>1;
      else
      {
        if (dh>dv)
          tmp=(outG[((i-1)*sx+j)*3]+outG[((i+1)*sx+j)*3])>>1;
        else
          tmp=(outG[(i*sx+j-1)*3]+outG[(i*sx+j+1)*3]+outG[((i-1)*sx+j)*3]+outG[((i+1)*sx+j)*3])>>2;
      }
      outG[(i*sx+j)*3] = tmp;
    }
  }
  // process RED channel
  for (i=1;i<sy-1;i+=2) // G-points (1/2)
  { 
    for (j=2;j<sx-1;j+=2)
    {
      tmp=outG[(i*sx+j)*3]+((outR[(i*sx+j-1)*3]-outG[(i*sx+j-1)*3]+
          outR[(i*sx+j+1)*3]-outG[(i*sx+j+1)*3])>>1);
      CLIP(tmp,outR[(i*sx+j)*3]);
    }
  }
  for (i=2;i<sy-2;i+=2)  
  {
    for (j=1;j<sx;j+=2) // G-points (2/2)
    {
      tmp=outG[(i*sx+j)*3]+((outR[((i-1)*sx+j)*3]-outG[((i-1)*sx+j)*3]+
          outR[((i+1)*sx+j)*3]-outG[((i+1)*sx+j)*3])>>1);
      CLIP(tmp,outR[(i*sx+j)*3]);
    }
    for (j=2;j<sx-1;j+=2) // B-points
    {
      tmp=outG[(i*sx+j)*3]+((outR[((i-1)*sx+j-1)*3]-outG[((i-1)*sx+j-1)*3]+
          outR[((i-1)*sx+j+1)*3]-outG[((i-1)*sx+j+1)*3]+
          outR[((i+1)*sx+j-1)*3]-outG[((i+1)*sx+j-1)*3]+
          outR[((i+1)*sx+j+1)*3]-outG[((i+1)*sx+j+1)*3])>>2);
      CLIP(tmp,outR[(i*sx+j)*3]);
    }
  }
  // process BLUE channel
  for (i=0;i<sy;i+=2)
  {
    for (j=1;j<sx-2;j+=2)
    {
      tmp=outG[(i*sx+j)*3]+((outB[(i*sx+j-1)*3]-outG[(i*sx+j-1)*3]+
          outB[(i*sx+j+1)*3]-outG[(i*sx+j+1)*3])>>1);
      CLIP(tmp,outB[(i*sx+j)*3]);
    }
  }
  for (i=1;i<sy-1;i+=2)
  {
    for (j=0;j<sx-1;j+=2)
    {
      tmp=outG[(i*sx+j)*3]+((outB[((i-1)*sx+j)*3]-outG[((i-1)*sx+j)*3]+
          outB[((i+1)*sx+j)*3]-outG[((i+1)*sx+j)*3])>>1);
      CLIP(tmp,outB[(i*sx+j)*3]);
    }
    for (j=1;j<sx-2;j+=2)
    {
      tmp=outG[(i*sx+j)*3]+((outB[((i-1)*sx+j-1)*3]-outG[((i-1)*sx+j-1)*3]+
          outB[((i-1)*sx+j+1)*3]-outG[((i-1)*sx+j+1)*3]+
          outB[((i+1)*sx+j-1)*3]-outG[((i+1)*sx+j-1)*3]+
          outB[((i+1)*sx+j+1)*3]-outG[((i+1)*sx+j+1)*3])>>2);
      CLIP(tmp,outB[(i*sx+j)*3]);
    }
  }
  
  // clear borders (to black)
  int w = 3;
  i=ws*w-1;
  j=dst->imageSize-1;
  while (i>=0) {
    dstdata[i--]=0;
    dstdata[j--]=0;
  }  
  i=ws*(sy-1)-1+w*3;
  while (i>sx) {
    j=6*w;
    while (j>0) {
      dstdata[i--]=0;
      j--;
    }
    i-=(sx-2*w)*3;
  }
}

// ************************************************************************** //
// Debayering method from coriander when downsampling. Adapted to work with
// IplImages and their 32bit row alignment. Expects RGGB pattern.
// ************************************************************************** //

void BayerDownsample(IplImage *src, IplImage *dst)
{
  int sxsrc = src->width;
  int sysrc = src->height;
  int sxdst = dst->width;
  int sydst = dst->height;
  if (sxsrc != 2*sxdst || sysrc != 2*sydst)
  {
    cerr << "Error: destination image must be half the size of source image" << endl
         << "       in function BayerDownsample!" << endl;
    exit(1);
  }
  int wssrc = src->widthStep;
  int wsdst = dst->widthStep;
  int srcpadding = wssrc + wssrc - sxsrc;
  int dstpadding = wsdst - sxdst * 3;
  
  // RGGB pattern expected
  unsigned char *dstdata = (unsigned char*)dst->imageData;
  unsigned char *srcdata = (unsigned char*)src->imageData;
  int rpos = 0;
  int gpos = 1;
  int gpos2 = wssrc;
  int bpos = wssrc + 1;
  int dstpos = 0;
  for (int i=0; i<sydst; i++)
  {
    for (int j=0; j<sxdst; j++)
    {
      int r = srcdata[rpos];
      int g = (srcdata[gpos] + srcdata[gpos2]) >> 1; // mean of the two G pixels
      int b = srcdata[bpos];
      dstdata[dstpos++] = b;
      dstdata[dstpos++] = g;
      dstdata[dstpos++] = r;
      rpos += 2;
      gpos += 2;
      gpos2 += 2;
      bpos += 2;
    }
    rpos += srcpadding;
    gpos += srcpadding;
    gpos2 += srcpadding;
    bpos += srcpadding;
    dstpos += dstpadding;
  }
}

// ************************************************************************** //
// Functions, Structs and Enums for parameter parsing
// ************************************************************************** //

typedef enum { OPENCV, NEAREST, SIMPLE, EDGESENSE, DOWNSAMPLE } DebayerMethod;
typedef enum { NONE, PNG, JPG, PIPE, STDOUT } OutputType;

// ************************************************************************** //

struct Parameters
{
  string input_filename;
  DebayerMethod deb_method;
  OutputType out_type;
  string output_filename;
  int crop_top, crop_bottom, crop_left, crop_right;
  int resize_width, resize_height;
  int skip_frames, max_frames;
  bool crop;
  bool resize;
  bool display;
  bool time;
  Parameters() : crop_top(0), crop_bottom(0), crop_left(0), crop_right(0),
                 resize_width(0), resize_height(0), skip_frames(0), max_frames(0),
                 crop(false), resize(false), display(false), time(false) {}
};

// ************************************************************************** //

void printUsageAndExit()
{
  cout << endl << "Usage: convert_raw_video <Filename> <Debayer Method> <Output Type> <Other Parameters>" 
       << endl << endl
       << "Debayer Method (mandatory):" << endl
       << "   opencv   (Method from OpenCV - fast but low quality)" << endl
       << "   nearest   (Nearest Neighbor Method - low quality)" << endl
       << "   simple   (Simple Interpolation - medium quality)" << endl
       << "   edgesense   (Edge Sense Method - slow but good quality)" << endl
       << "   downsample   (Downsampling Method - best quality but reduced size)" << endl
       << endl
       << "Output Types (mandatory):" << endl
       << "   none   (No output, useful in combination with display or time parameters)" << endl
       << "   png:<filename prefix>   (Output as lossless png file sequence)" << endl
       << "   jpg:<filename prefix>   (Output as lossy jpg file sequence)" << endl
       << "   pipe:<filename>   (Output as raw video stream to named pipe)" << endl
       << "   stdout   (Output as raw video stream to stdout)" << endl
       << endl
       << "Other Parameters (optional):" << endl
       << "   crop:<top>:<bottom>:<left>:<right>   (crop image borders)" << endl
       << "   resize:<width>:<height>   (resize image)" << endl
       << "   skip:<num of frames to skip>:<max frames to process>   (limit frames)" << endl
       << "   display   (display images while processing)" << endl
       << "   time   (measure time)" << endl
       << endl 
       << "Order of parameters is important (except optional parameters)!" << endl << endl;
  exit(0);
}

// ************************************************************************** //

vector<string> splitString(string str)
{
  vector<string> strings;
  string sep_str;
  for (unsigned int i=0; i<str.length(); i++)
  {
    if (str[i] != ':')
    {
      sep_str = sep_str + str[i];
    }
    else
    {
      strings.push_back(sep_str);
      sep_str.clear();
    }
  }
  if (sep_str.length())
    strings.push_back(sep_str);
  return strings;
}

// ************************************************************************** //

void setParameters(Parameters& p, const vector<string>& str)
{
  if (str.size() < 3) printUsageAndExit();
  // set filename
  p.input_filename = str[0];
  // set debayer method
  if (str[1] == "opencv") p.deb_method = OPENCV;
  else if (str[1] == "nearest") p.deb_method = NEAREST;
  else if (str[1] == "simple") p.deb_method = SIMPLE;
  else if (str[1] == "edgesense") p.deb_method = EDGESENSE;
  else if (str[1] == "downsample") p.deb_method = DOWNSAMPLE;
  else printUsageAndExit();
  // set output method
  vector<string> split_strings = splitString(str[2]);
  if (split_strings[0] == "none")
  {
    p.out_type = NONE;
  }
  else if (split_strings[0] == "png")
  {
    p.out_type = PNG;
    if (split_strings.size() != 2) printUsageAndExit();
    p.output_filename = split_strings[1];
  }
  else if (split_strings[0] == "jpg")
  {
    p.out_type = JPG;
    if (split_strings.size() != 2) printUsageAndExit();
    p.output_filename = split_strings[1];
  }
  else if (split_strings[0] == "pipe")
  {
    p.out_type = PIPE;
    if (split_strings.size() != 2) printUsageAndExit();
    p.output_filename = split_strings[1];
  }
  else if (split_strings[0] == "stdout")
  {
    p.out_type = STDOUT;
  }
  else printUsageAndExit();
  // set optional parameters
  for (unsigned int i=3; i<str.size(); i++)
  {
    split_strings = splitString(str[i]);
    if (split_strings[0] == "crop")
    {
      if (split_strings.size() != 5) printUsageAndExit();
      p.crop = true;
      p.crop_top = atoi(split_strings[1].c_str());
      p.crop_bottom = atoi(split_strings[2].c_str());
      p.crop_left = atoi(split_strings[3].c_str());
      p.crop_right = atoi(split_strings[4].c_str());
    }
    else if (split_strings[0] == "resize")
    {
      if (split_strings.size() != 3) printUsageAndExit();
      p.resize = true;
      p.resize_width = atoi(split_strings[1].c_str());
      p.resize_height = atoi(split_strings[2].c_str());
    }
    else if (split_strings[0] == "skip")
    {
      if (split_strings.size() != 3) printUsageAndExit();
      p.skip_frames = atoi(split_strings[1].c_str());
      p.max_frames = atoi(split_strings[2].c_str());
    }
    else if (split_strings[0] == "display")
    {
      p.display = true;
    }
    else if (split_strings[0] == "time")
    {
      p.time = true;
    }
  }
}

#include <fstream>

unsigned int numberOfFramesInAvi(const char* path2Video)
{
	int  nFrames;
	char tempSize[4];

	// Trying to open the video file
	ifstream  videoFile( path2Video , ios::in | ios::binary );
	// Checking the availablity of the file
	if ( !videoFile ) {
		cout << "Couldn’t open the input file " << path2Video << endl;
		exit( 1 );
	}

	// get the number of frames
	videoFile.seekg( 0x30 , ios::beg );
	videoFile.read( tempSize , 4 );
	nFrames = (unsigned char) tempSize[0]
		+ 0x100*(unsigned char) tempSize[1]
		+ 0x10000*(unsigned char) tempSize[2]
		+ 0x1000000*(unsigned char) tempSize[3];

	videoFile.close();
	return nFrames;
}



// ************************************************************************** //
// Main Function
// ************************************************************************** //

int main(int argc, char** argv)
{ 
  Parameters p;
  vector<string> str;
  char filename[1024];
  CvCapture* capture = NULL;
  FILE* file = NULL;
  struct timeval stime, etime;
  IplImage* frame = NULL;
  IplImage* output = NULL;
  IplImage* image_bayer = NULL;
  IplImage* image_debayer = NULL;
  IplImage* image_crop = NULL;
  IplImage* image_resize = NULL;
  
  // parse command line for parameters
  for (int i=1; i<argc; i++)
    str.push_back(string(argv[i]));
  setParameters(p, str);
  
  // initialize capturing
  capture = cvCaptureFromAVI(p.input_filename.c_str());
  if(!capture)
  {
    cerr << "Error: Could not initialize capturing..." << endl
         << "       Possibly wrong filename: " << p.input_filename << endl;
    exit(1);
  }
  
  // open pipe or stdout for output
  if (p.out_type == PIPE)
  {
    file = fopen(p.output_filename.c_str(), "wb");
    if (!file)
    {
      cerr << "Error: Could not open named pipe " << p.output_filename << endl;
      exit(1);
    }
  }
  else if (p.out_type == STDOUT)
  {
    file = stdout;
  }
  
  // create display
  if (p.display)
  {
    cvNamedWindow("CameraCapture", 1);
  }
  
  // grab first frame
  frame = cvQueryFrame(capture);
  int frame_count = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);
  int act_frame = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES);
  CvSize full_size = cvGetSize(frame);
  CvSize half_size = full_size;
  CvSize crop_size;
  CvSize output_size;
  half_size.width /= 2;
  half_size.height /= 2;

/*cerr << (int) cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT) << endl;
cerr << (int) cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH) << endl;
cerr <<  (int) cvGetCaptureProperty(capture, CV_CAP_PROP_FPS) << endl;
cerr <<  (int) cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT) << endl;
cerr << numberOfFramesInAvi(p.input_filename.c_str()) << endl;*/

frame_count = numberOfFramesInAvi(p.input_filename.c_str());

  
  // allocate necessary images
  image_bayer = cvCreateImage(full_size, IPL_DEPTH_8U, 1);
  if (p.deb_method == DOWNSAMPLE)
    image_debayer = cvCreateImage(half_size, IPL_DEPTH_8U, 3);
  else
    image_debayer = cvCreateImage(full_size, IPL_DEPTH_8U, 3);
  output_size = crop_size = cvGetSize(image_debayer);
  if (p.crop)
  {
    crop_size.width -= (p.crop_left + p.crop_right);
    crop_size.height -= (p.crop_top + p.crop_bottom);
    image_crop = cvCreateImage(crop_size, IPL_DEPTH_8U, 3);
    output_size = crop_size;
  }
  if (p.resize)
  {
    output_size = cvSize(p.resize_width, p.resize_height);
    image_resize = cvCreateImage(output_size, IPL_DEPTH_8U, 3);
  }
  
  // Output some useful information here 
  cerr << "Original video size: " << full_size.width << "x" << full_size.height << endl;
  cerr << "Original frame count: " << frame_count << endl;
  cerr << "Output video size: " << output_size.width << "x" << output_size.height << endl;
  cerr << "Output frame size in bytes: " << output_size.width * output_size.height * 3 << endl;

  // skip frames at the beginning  
  while (act_frame < p.skip_frames)
  {
    // grab next frame
    frame = 0;
    frame = cvQueryFrame(capture);
    if(!frame)
    {
      cerr << "ERROR: Couldn't grab frame!" << endl;
      exit(1);
    }
    act_frame = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES);
  }
  if (frame_count < p.skip_frames + p.max_frames)
  {
    cerr << "WARNING: Video framecount insufficient for frame limit given with skip parameter!"
         << endl;
  }

  if (p.max_frames == 0)
    p.max_frames = frame_count - p.skip_frames;

  while (act_frame < (p.skip_frames + p.max_frames))
  {
    if (p.time) gettimeofday(&stime, NULL);
      
    // debayer actual image
    if (frame->nChannels == 3) // for some reason, raw images are grabbed with 3 channels
    {
      cvSetImageCOI(frame, 1);
      cvCopy(frame, image_bayer);
      // debayer image
      if (p.deb_method == OPENCV)
        cvCvtColor(image_bayer, image_debayer, CV_BayerBG2BGR);
      else if (p.deb_method == NEAREST)
        BayerNearestNeighbor(image_bayer, image_debayer);
      else if (p.deb_method == SIMPLE)
        BayerSimple(image_bayer, image_debayer);
      else if (p.deb_method == EDGESENSE)
        BayerEdgeSense(image_bayer, image_debayer);
      else if (p.deb_method == DOWNSAMPLE)
        BayerDownsample(image_bayer, image_debayer);
    }
    else
    {
      // debayer image
      if (p.deb_method == OPENCV)
        cvCvtColor(frame, image_debayer, CV_BayerBG2BGR);
      else if (p.deb_method == NEAREST)
        BayerNearestNeighbor(frame, image_debayer);
      else if (p.deb_method == SIMPLE)
        BayerSimple(frame, image_debayer);
      else if (p.deb_method == EDGESENSE)
        BayerEdgeSense(frame, image_debayer);
      else if (p.deb_method == DOWNSAMPLE)
        BayerDownsample(frame, image_debayer);
    }
    output = image_debayer;
    
    // crop image
    if (p.crop)
    {
      CvRect roi = cvRect(p.crop_left, p.crop_top, crop_size.width, crop_size.height);
      cvSetImageROI(image_debayer, roi);
      cvCopy(image_debayer, image_crop);
      cvResetImageROI(image_debayer);
      output = image_crop;
    }
    // resize image
    if (p.resize)
    {
      cvResize(output, image_resize, CV_INTER_CUBIC);
      output = image_resize;
    }
    
    // output image
    if (p.out_type == PNG)
    {
      sprintf(filename, "%s%05d.png", p.output_filename.c_str(), act_frame - p.skip_frames);
      cvSaveImage(filename, output);
    }
    else if (p.out_type == JPG)
    {
      sprintf(filename, "%s%05d.jpg", p.output_filename.c_str(), act_frame - p.skip_frames);
      cvSaveImage(filename, output);
    }
    else if (p.out_type == PIPE || p.out_type == STDOUT)
    {
      if (output->width % 4 == 0)
        fwrite(output->imageData, 1, output->imageSize, file);
      else // remove padding at end of lines (32 bit alignment in OpenCV!)
        for (int i=0; i<output->height; i++)
          fwrite(output->imageData+i*output->widthStep, 1, output->width*3, file);
    }
    
    // do GUI processing
    if (p.display)
    {
      // display output image
      cvShowImage("CameraCapture", output);
      
      int c = cvWaitKey(10);
      // exit when ESC is pressed
      if((char) c == 27)
        break;
      // pause if SPACE is pressed
      else if ((char) c == 32)
      {
        while (true)
        {
          c = cvWaitKey(10);
          // unpause if SPACE is pressed again
          if((char) c == 32)
            break;
        }
      }
    }
    
    // stop time
    if (p.time)
    {
      gettimeofday(&etime, NULL);
      double usecs = (double)(etime.tv_usec - stime.tv_usec);
      double secs  = (double)(etime.tv_sec  - stime.tv_sec);
      double ms = 1000.0 * secs + 0.001 * usecs;
      cerr << "Processing time for frame " << act_frame << ": " << ms << "ms" << endl; 
    }
    
    // grab next frame
    frame = 0;
    frame = cvQueryFrame(capture);
    if(!frame)
      break;
    act_frame = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES);
  }  
  
  // cleanup
  cvReleaseCapture(&capture);
  if (p.display)
    cvDestroyWindow("CameraCapture");
  if (image_bayer)
    cvReleaseImage(&image_bayer);
  if (image_debayer)
    cvReleaseImage(&image_debayer);
  if (image_crop)
    cvReleaseImage(&image_crop);
  if (image_resize)
    cvReleaseImage(&image_resize);
  if (p.out_type == PIPE)
    fclose(file);
  
  return 0;
}

