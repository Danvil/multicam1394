#!/bin/sh

VIDEOCOUNT=5
INPUTPREFIX=raw
OUTPUTPREFIX=camsmall
ORI_W=780
ORI_H=582
NEW_W=384
NEW_H=288
FPS=25
BITRATE=8000

for ((i = 0; i < $VIDEOCOUNT; i++))
do
  ./convert_raw_video ${INPUTPREFIX}${i}.avi downsample stdout crop:1:2:3:3 | mencoder - \
  -demuxer rawvideo -rawvideo w=${NEW_W}:h=${NEW_H}:fps=${FPS}:format=bgr24 -ovc lavc \
  -lavcopts vcodec=mpeg4:mbd=2:trell:v4mv:vbitrate=${BITRATE}:vpass=1 -o ${OUTPUTPREFIX}${i}.avi
  
  ./convert_raw_video ${INPUTPREFIX}${i}.avi downsample stdout crop:1:2:3:3 | mencoder - \
  -demuxer rawvideo -rawvideo w=${NEW_W}:h=${NEW_H}:fps=${FPS}:format=bgr24 -ovc lavc \
  -lavcopts vcodec=mpeg4:mbd=2:trell:v4mv:vbitrate=${BITRATE}:vpass=2 -o ${OUTPUTPREFIX}${i}.avi

  rm divx2pass.log
done

