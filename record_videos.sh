#!/bin/bash
# first command line parameter is number of cameras

# change the following vars if necessary
VIDEOCOUNT=3
PIPEPREFIX=/tmp/ias_video_pipe
OUTPUTPREFIX=raw
IMG_W=640
IMG_H=480
FPS=60
PACKETSIZE=2448
BITRATE=8000

# don't change anything from here unless you know what you do
if [ "$1" != "" ]; then
  VIDEOCOUNT=$1;
fi
VIDINDICES=
for ((i = 0; i < VIDEOCOUNT; i++))
do
  if [ ! -p ${PIPEPREFIX}${i} ]; then
    mkfifo ${PIPEPREFIX}${i};
  fi
  VIDINDICES+="$i ";
done

for ((i = 0; i < VIDEOCOUNT; i++))
do
  mencoder -demuxer rawvideo -rawvideo w=${IMG_W}:h=${IMG_H}:fps=${FPS}:y8 -ovc lavc -lavcopts \
  vcodec=mpeg4:vbitrate=${BITRATE} ${PIPEPREFIX}${i} \
  -o ${OUTPUTPREFIX}${i}.avi &
done

ias_capture_raw_video -i $VIDEOCOUNT $VIDINDICES -d $VIDEOCOUNT 4 30 -l -c ${PIPEPREFIX} -p ${PACKETSIZE} 

