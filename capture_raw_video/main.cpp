/********* ********** ********** ********** ********** ********** ********** **********
 *
 * capture raw video
 * 
 * Authors: Jan Bandouch (bandouch@weikersd.in.tum), David Weikersdorfer (weikersd@in.tum.de)
 *          Integrated Autonomous Systems
 *          Technisch Universität München
 *
 * Opens up 1394 camera streams using libdc1934 and forwards video data to named pipes.
 *
 ********* ********** ********** ********** ********** ********** ********** *********/

#include <stdio.h>
#include <stdint.h>
#include <dc1394/dc1394.h>
#include <stdlib.h>
#include <inttypes.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <dc1394/dc1394.h>
#include <sys/time.h>

enum ColorMode {
	Mono8,	/** One 8-bit value per pixel (Greyscale). Corresponds to DC1394_COLOR_CODING_MONO8. */
	Rgb8,	/** Three 8-bit values per pixel (RGB). Corresponds to DC1394_COLOR_CODING_RGB8. */
	Bayer8	/** Four 8-bit values per pixel in a square (RGGB). Corresponds to DC1394_COLOR_CODING_RAW8. */
};


#include "display.h"
#include <cstdlib>

using namespace std;

int DisplayWindow::m_event_count = 0;   //!< HACK: Define static member from display.h here

/** Global parameters */
vector<int> g_cam_idx;              //!< indices of requested cameras
int g_disp_scale = 1;               //!< reduce scale for display (powers of 2)
int g_disp_count = -1;				//!< number of cameras to be displayed
int g_disp_every_xth_frame = 10;	//!< every xth frame will be displayed
int g_width = 640;					//!< image width
int g_height = 480;					//!< image heigth
int g_xpos = 0;                     //!< image x offset (format7)
int g_ypos = 0;                     //!< image y offset (format7)
int g_bytes_per_packet = 1448;      //!< number of bytes per packet in FORMAT7 -> framerate
string g_pipe_prefix;               //!< Filename prefix for named pipes (if empty no capture)
bool g_load_memory = false;         //!< whether saved memory settings should be loaded
bool g_use_trigger = false;			// whether to use external trigger like in the ias kitchen
bool g_use_800_b = true;			// whether to use iso speed 800 and operation mode Bs
ColorMode g_color_mode = Mono8;



/** Global function for cleaning up allocated memory and exiting. */
void cleanup_and_exit(dc1394_t *dc1394, vector<dc1394camera_t*>& cameras)
{
	for (unsigned int i=0; i<g_cam_idx.size(); i++) {
		dc1394_video_set_transmission(cameras[g_cam_idx[i]], DC1394_OFF);
		dc1394_capture_stop(cameras[g_cam_idx[i]]);
	}
	for (unsigned int i=0; i<cameras.size(); i++) {
		dc1394_camera_free(cameras[i]);
	}
	dc1394_free(dc1394);
	exit(1);
}


/** In case no command line parameters were specified, print usage and exit. */
void print_usage_and_exit()
{
	cerr << endl << "Usage: " << endl
		<< "   capture_raw_video " << endl
		<< endl
		<< "Optional Camera Parameters (give number of cameras to use and camera index order):" << endl
		<< "   -i <# cameras> <index first cam> ... <index last cam>" << endl
		<< "      default: all found cameras in found order" << endl
		<< endl
		<< "Optional Display Parameters (append either all or none in this order):" << endl
		<< "   -d <# cameras> <reduce scale factor (1,2,4,8)> <display every xth frame only>" << endl
		<< "      default: display all cameras at full size and every 5th frame" << endl
		<< endl
		<< "Optional Format Parameters (append either all or none in this order):" << endl
		<< "   -f <format7 width> <format7 height> <format7 x_pos> <format7 y_pos>" << endl
		<< endl
		<< "Optional Operation mode Parameter to use legacy operation mode with ISO bandwidth 400:" << endl
		<< "   -op400 (default: op800)" << endl
		<< endl
		<< "Optional Packet Parameter (this influences the framerate):" << endl
		<< "   -p <packet size>" << endl
		<< "      default: 1448" << endl
		<< endl
		<< "Optional Parameter for loading saved settings from camera memory:" << endl
		<< "   -l (default: disabled)" << endl
		<< endl
		<< "Optional Capture Parameter (enables capturing through named pipes):" << endl
		<< "   -c <named pipe filename prefix>" << endl
		<< "      default: no pipes used - capture disabled" << endl
		<< endl
		<< "Optional External Trigger (enables external trigger using camera with index 0 as clock):" << endl
		<< "   -t (default: disabled)" << endl;
	exit(0);
}


/** Function for parsing the command line. */
void parse_command_line(int argc, char *argv[])
{
	int cl_idx = 1;
	// parse number and index of requested cameras
//	g_cam_idx.resize(atoi(argv[cl_idx++]));
//	if (argc < (cl_idx + g_cam_idx.size()))
//		print_usage_and_exit();
//	for (unsigned int i=0; i<g_cam_idx.size(); i++)
//		g_cam_idx[i] = atoi(argv[cl_idx++]);
	// parse optional parameters
	while (cl_idx < argc) {
		if (strcmp(argv[cl_idx], "-i") == 0) {
			cl_idx++;
			//  number of cameras and indices
			int cnt = atoi(argv[cl_idx++]);
			if (argc <= (cl_idx + cnt - 1)) {
				print_usage_and_exit();
			}
			for(int i=0; i<cnt; i++) {
				g_cam_idx.push_back(atoi(argv[cl_idx++]));
			}
		} 
		else if (strcmp(argv[cl_idx], "-d") == 0) {
			// display parameters
			if (argc <= (cl_idx + 3)) {
				print_usage_and_exit();
			}
			cl_idx++;
			g_disp_count = atoi(argv[cl_idx++]);
			g_disp_scale = atoi(argv[cl_idx++]);
			g_disp_every_xth_frame = atoi(argv[cl_idx++]);
			if (!(g_disp_scale==1 || g_disp_scale==2 || g_disp_scale==4 || g_disp_scale==8)) {
				cerr << "ERROR: Display reduce scale factor must be 1, 2, 4 or 8" << endl;
				print_usage_and_exit();
			}
		}
		else if (strcmp(argv[cl_idx], "-f") == 0) {
			// format parameters
			if (argc <= (cl_idx + 4)) {
				print_usage_and_exit();
			}
			cl_idx++;
			g_width = atoi(argv[cl_idx++]);
			g_height = atoi(argv[cl_idx++]);
			g_xpos = atoi(argv[cl_idx++]);
			g_ypos = atoi(argv[cl_idx++]);
		}
		else if (strcmp(argv[cl_idx], "-op400") == 0) {
			cl_idx++;
			g_use_800_b = false;			
		}
		else if (strcmp(argv[cl_idx], "-p") == 0) {
			// packet size
			if (argc <= (cl_idx + 1)) {
				print_usage_and_exit();
			}
			cl_idx++;
			g_bytes_per_packet = atoi(argv[cl_idx++]);
		}
		else if (strcmp(argv[cl_idx], "-c") == 0) {
			// capture pipe name
			if (argc <= (cl_idx + 1)) {
				print_usage_and_exit();
			}
			cl_idx++;
			g_pipe_prefix = argv[cl_idx++];
		}
		else if (strcmp(argv[cl_idx], "-l") == 0) {
			// camera memory use parameter
			cl_idx++;
			g_load_memory = true;
		}
		else if(strcmp(argv[cl_idx], "-t") == 0) {
			cl_idx++;
			g_use_trigger = true;
		}
		else {
			print_usage_and_exit();
		}
	}
}


/** Main function */
int main (int argc, char *argv[])
{

	// ************************************************************************ //
	// *** INITALIZATION STUFF ************************************************ //
	// ************************************************************************ //

	// get command line parameters
	parse_command_line(argc, argv);
	bool capture = !g_pipe_prefix.empty();
	
	// check that packet size is a multiple of 4
	if(g_bytes_per_packet % 4 != 0) {
		cerr << "Packet size is not a multiple of 4! (Was: " << g_bytes_per_packet << ")" << endl;
		exit(1);
	}

	// init dc1394
	dc1394_t *dc1394=dc1394_new();

	// init cameras
	dc1394camera_list_t *camera_list;
	dc1394camera_t *camera;
	dc1394error_t err = dc1394_camera_enumerate(dc1394, &camera_list);
	unsigned int found_cam_count = camera_list->num;
	DC1394_ERR_RTN(err, "Failed to enumerate cameras");
	if (found_cam_count == 0) {
		cerr << "No cameras found" << endl;
		exit(1);
	} else {
		// print info about found cameras
	    uint32_t i;
    	printf("Found %d camera(s): \n", found_cam_count);
    	for(i=0; i<found_cam_count; i++) {
       		printf("\t%d: guid %llu\n", i, camera_list->ids[i].guid);
   		}
   	}

	// if command line parameters do not force camera count or order we use all found cameras in the found order
	if(g_cam_idx.size() == 0) {
		for(int i=0; i<found_cam_count; i++) {
			g_cam_idx.push_back(i);
		}
	} else {
		if(found_cam_count < g_cam_idx.size() ) {
			cerr << "Requested " << g_cam_idx.size() << " cameras, but found only " << found_cam_count << endl;
			exit(1);
		}
	}
	int cam_count = g_cam_idx.size();

	cout << "Initializing cameras ... " << endl;
	if (g_load_memory) {
		cout << "\tUsing user settings from camera memory" << endl;
	}
	if(g_use_trigger) {
		cout << "\tUsing external trigger like in the IAS kitchen" << endl;
	}
	
	vector<dc1394camera_t*> cameras(found_cam_count);
	vector<dc1394featureset_t> features(found_cam_count);
	vector<dc1394format7modeset_t> modesets(found_cam_count);
	for (unsigned int i=0; i<found_cam_count; i++) {
		camera = dc1394_camera_new(dc1394, camera_list->ids[i].guid);
		if (!camera) {
			cerr << "Failed to initialize cam " << i << endl;
			exit(1);
		}
		cameras[i] = camera;
		// get camera features
		if (dc1394_feature_get_all(camera, &features[i]) != DC1394_SUCCESS) {
			cerr << "Could not get camera " << i << "s feature information!" << endl;
		}
		// else {
		//   dc1394_feature_print_all(&features[i], stdout);
		// }
		// check format 7 capabilities
		if (dc1394_format7_get_modeset(camera, &modesets[i]) != DC1394_SUCCESS) {
			cerr << "Could not query Format_7 informations on cam " << i << endl;
		}
		// load memory settings (I hardcoded channel 1 for our AVT cameras!!!)
		if (g_load_memory) {
			if (dc1394_memory_load(camera, 1) != DC1394_SUCCESS) {
				cerr << "Could not load user settings from memory on cam " << i << endl;
			}
		}
		if(g_use_trigger) {
			// set all except the first camera to external trigger
			// (according to the hardware settings in the kitchen)
			if (i != 0) {
				if (dc1394_feature_set_power(camera, DC1394_FEATURE_TRIGGER, DC1394_ON) != DC1394_SUCCESS) {
					cerr << "Could not activate external trigger on cam " << i << endl;
				}
			}
		}
	}
	dc1394_camera_free_list(camera_list);

	// init named pipes for output
	vector<FILE*> pipe(cam_count);
	if (capture) {
		cout << "Initializing pipes ... " << endl;
		for (int i=0; i<cam_count; i++) {
			char pipename[256];
			sprintf(pipename, "%s%d", g_pipe_prefix.c_str(), i);
			pipe[i] = fopen(pipename, "wb");
			if (pipe[i] == NULL) {
				cerr << "Could not open named pipe for capturing: " << pipename << endl;
				cleanup_and_exit(dc1394, cameras);
			}
		}
	}

	// init timestamps file
	FILE* filestamps = 0;
	if(capture) {
		filestamps = fopen("timestamps.log", "w");
	}

	// ************************************************************************ //
	// *** LOAD AND SET MODES ************************************************* //
	// ************************************************************************ //

	cout << "Setting modes ... " << endl;
	if(g_use_800_b) {
		cout << "Using iso speed 800 in operation mode B" << endl;
	} else {
		cout << "Using iso speed 400 in normal mode" << endl;
	}
	cout << "Using video format FORMAT7_0" << endl;
	cout << "Using image size " << g_width << "x" << g_height << "" << endl;
	switch(g_color_mode) {
	case Mono8: cout << "Using MONO8 color encoding" << endl; break;
	case Rgb8: cout << "Using RGB8 color encoding" << endl; break;
	case Bayer8: cout << "Using RAW8 color encoding" << endl; break;
	default: cerr << "Unkown color encoding!" << endl; break;
	}
	cout << "Using " << g_bytes_per_packet << " bytes per package" << endl;

	for (unsigned int i=0; i<cameras.size(); i++) {
		if(g_use_800_b) {
			// operational mode b with 800 MBit
			if(dc1394_video_set_operation_mode(cameras[i], DC1394_OPERATION_MODE_1394B) != DC1394_SUCCESS) {
				cerr << "Could not set operation mode B for cam " << i << endl;
				cleanup_and_exit(dc1394, cameras);
			}

			if (dc1394_video_set_iso_speed(cameras[i], DC1394_ISO_SPEED_800) != DC1394_SUCCESS) {
				cerr << "Could not set iso speed 800 for cam " << i << endl;
				cleanup_and_exit(dc1394, cameras);
			}
		} else {
			// normal mode with 400 MBit
			if(dc1394_video_set_operation_mode(cameras[i], DC1394_OPERATION_MODE_LEGACY) != DC1394_SUCCESS) {
				cerr << "Could not set normal operation mode for cam " << i << endl;
				cleanup_and_exit(dc1394, cameras);
			}

			if (dc1394_video_set_iso_speed(cameras[i], DC1394_ISO_SPEED_400) != DC1394_SUCCESS) {
				cerr << "Could not set iso speed 400 for cam " << i << endl;
				cleanup_and_exit(dc1394, cameras);
			}
		}

		if (dc1394_video_set_mode(cameras[i], DC1394_VIDEO_MODE_FORMAT7_0) != DC1394_SUCCESS) {
			cerr << "Could not set video mode Format7_0 for cam " << i << endl;
			cleanup_and_exit(dc1394, cameras);
		}

		if (dc1394_format7_set_image_size(cameras[i], DC1394_VIDEO_MODE_FORMAT7_0,  g_width, g_height) != DC1394_SUCCESS) {
			cerr << "Could not set Format7 image size for cam " << i << endl;
			cleanup_and_exit(dc1394, cameras);
		}

		if (dc1394_format7_set_image_position(cameras[i], DC1394_VIDEO_MODE_FORMAT7_0, g_xpos, g_ypos) != DC1394_SUCCESS) {
			cerr << "Could not set Format7 image position for cam " << i << endl;
			cleanup_and_exit(dc1394, cameras);
		}

		dc1394color_coding_t cm;
		switch(g_color_mode) {
		case Mono8: cm = DC1394_COLOR_CODING_MONO8; break;
		case Rgb8: cm = DC1394_COLOR_CODING_RGB8; break;
		case Bayer8: cm = DC1394_COLOR_CODING_RAW8; break;
		};
		if (dc1394_format7_set_color_coding(cameras[i], DC1394_VIDEO_MODE_FORMAT7_0, cm) != DC1394_SUCCESS) {
			cerr << "Could not set color coding for cam " << i << endl;
			cleanup_and_exit(dc1394, cameras);
		}

		if (dc1394_format7_set_packet_size(cameras[i], DC1394_VIDEO_MODE_FORMAT7_0, g_bytes_per_packet) != DC1394_SUCCESS) {
			cerr << "Could not set " << g_bytes_per_packet << " bytes per packet for cam " << i << endl;
			cleanup_and_exit(dc1394, cameras);
		}
	}


	// ************************************************************************ //
	// *** CAPTURE ************************************************************ //
	// ************************************************************************ //

	// start camera transmissions
	for (int i=0; i<cam_count; i++) {
		dc1394camera_t* camera = cameras[g_cam_idx[i]];
		if (dc1394_capture_setup(camera, 10, DC1394_CAPTURE_FLAGS_DEFAULT) != DC1394_SUCCESS) {
			cerr
				<< "Could not setup cam " << g_cam_idx[i] << endl 
				<< "Make sure that the video mode and framerate are supported" << endl;
			cleanup_and_exit(dc1394, cameras);
		}

		if (dc1394_video_set_transmission(camera, DC1394_ON) != DC1394_SUCCESS) {
			cerr << "Could not start iso transmission on cam " << g_cam_idx[i] << endl; 
			cleanup_and_exit(dc1394, cameras);
		}
	}

	// init fltk windows
	if(g_disp_count == -1) {
		g_disp_count = cam_count;
	}
	vector<DisplayWindow*> window(g_disp_count);
	for (int i=0; i<g_disp_count; i++) {
		char wintitle[16];
		sprintf(wintitle, "Camera %d", g_cam_idx[i]);
		window[i] = new DisplayWindow(g_width, g_height, g_disp_scale, g_color_mode, wintitle, i*(g_width/g_disp_scale+10), 50);
		window[i]->show();
	}

	bool capture_running = false;
	if (capture) { // if capture, wait for key or button press
		cout << endl << "Everything set up! Press inside window to start capturing!" << endl;
	}
	else { // if only display without capture
		cout
			<< endl << "Displaying camera images! Press inside window to exit program!"
			<< endl;
	}

	// grab and display images in a loop
	vector<dc1394video_frame_t*> frame(cam_count);
	timeval start, end, start2, end2;
	gettimeofday(&start, 0);
	int count = 0;
	timeval fps_tick = start;
	int fps_frame_count = 0;
	bool first_framerate_estimate = false;
	while (true) {
		for (int i=0; i<cam_count; i++) {
			dc1394camera_t* camera = cameras[g_cam_idx[i]];
			// reserve frame from buffer
			// gettimeofday(&start2, 0);
			if (dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame[i]) != DC1394_SUCCESS) {
				cerr << "Error while grabbing images from cam " << g_cam_idx[i] << endl; 
				cleanup_and_exit(dc1394, cameras);
			}
			// gettimeofday(&end2, 0);
			// cerr << i << " Blocking: " << ((end2.tv_sec - start2.tv_sec) * 1000.0 + (end2.tv_usec - start2.tv_usec) / 1000.0) << endl;

			// output data to named pipes
			// gettimeofday(&start2, 0);
			if (capture && capture_running) {
				fwrite(frame[i]->image, frame[i]->image_bytes, 1, pipe[i]);
				fflush(pipe[i]);
				if (i == 0) {
					fprintf(filestamps, "%llu\n", frame[0]->timestamp);
				}
			}
			// gettimeofday(&end2, 0);
			// cerr << i << " Writing: " << ((end2.tv_sec - start2.tv_sec) * 1000.0 + (end2.tv_usec - start2.tv_usec) / 1000.0) << endl;

			// draw every xth frame
			if (i<g_disp_count && count%g_disp_every_xth_frame == 0) {
				cout << "Frame for camera " << i << " with " << frame[i]->image_bytes << " bytes" << endl;
				window[i]->provideFrame(frame[i]->image);
				window[i]->redraw();
				// cerr << "Timestamp for Cam " << g_cam_idx[i] << ": " << frame[i]->timestamp << endl;
			}

			// free frame in buffer
			if (dc1394_capture_enqueue(camera, frame[i]) != DC1394_SUCCESS) {
				cerr << "Error while releasing frame from cam " << g_cam_idx[i] << endl; 
				cleanup_and_exit(dc1394, cameras);
			}

			// keep GUI responsive
			Fl::wait(0.0);
		}
		
		count++;
		
		// process user commands
		if (capture) {
			if (!capture_running && DisplayWindow::getEventCount()) { // first press
				capture_running = true;
				cout
					<< endl
					<< "Capture started! Press inside window to stop and exit program!" << endl;
			}
			else if (DisplayWindow::getEventCount() > 1) { // second press
				cout
					<< endl
					<< "Capture stopped successfully!" << endl;
				break;
			}
		}
		else if (DisplayWindow::getEventCount()) { // no capture mode and sth pressed
			cout
				<< endl
				<< "Stopped displaying images!" << endl;
			break;
		}
		/*else if (!first_framerate_estimate) {
			// get a first framerate estimate after 5 seconds
			gettimeofday(&end, 0);
			double time = ((end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0);
			if (time > 5.0) {
				cout
					<< endl
					<< "Estimated Framerate: " << (double)count / time << endl;
				first_framerate_estimate = true;
			}
		}*/
		
		// print framerate in window title (update every 1 second)
		gettimeofday(&end, 0);
		double time = ((end.tv_sec - fps_tick.tv_sec) + (end.tv_usec - fps_tick.tv_usec) / 1000000.0);
		if (time > 1.0) {
			fps_tick = end;
			double fps = (double)(count - fps_frame_count) / time;
			fps_frame_count = count;
			double fps_rnd = (double)((int)(fps*100.0)) / 100.0;
			for (int i=0; i<g_disp_count; i++) {
				char wintitle[128];
				sprintf(wintitle, "Camera %d - %f fps", g_cam_idx[i], fps);
				window[i]->label(wintitle);
			}
		}
	}

	// calculate frame rates
	gettimeofday(&end, 0);
	double time = ((end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0);
	double fps = (double)count / time;
	cout
		<< endl << "Time: " << time << " sec;\tAverage Framerate: " << fps << endl
		<< endl;


	// ************************************************************************ //
	// *** CLEANUP ************************************************************ //
	// ************************************************************************ //

	if (capture) {
		cout << "Closing pipes..." << endl;
		for (unsigned int i=0; i<pipe.size(); i++) {
			fclose(pipe[i]);
		}
		fclose(filestamps);
	}

	for (int i=0; i<g_disp_count; i++) {
		delete window[i];
	}

	cout << "Cleaning up dc1394..." << endl;
	cleanup_and_exit(dc1394, cameras);

	return 0; // wont get here ...
}

