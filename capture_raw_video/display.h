#ifndef __CAPTURE_RAW_VIDEO_DISPLAY_H__
#define __CAPTURE_RAW_VIDEO_DISPLAY_H__

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <cstring>

/** Derived FLTK display class for drawing captured images */
class DisplayWindow
: public Fl_Window
{
private:
    unsigned char*  mp_img;         //!< scaled image data for display
    int             m_width;        //!< display and scaled image width
    int             m_height;       //!< display and scaled image height
    int             m_orig_width;   //!< original image width
    int             m_orig_height;  //!< original image height
    int             m_scale;        //!< reduce scale factor for image display (is power of 2)
    ColorMode		m_mode;
    bool			m_mono;
    static int      m_event_count;  //!< Number of times a key or button has been pressed
    
public:
    
    /** Constructor */
    DisplayWindow(int width, int height, int scale, ColorMode mode, const char* title)
    : Fl_Window(0, 0, (int)(width/scale), (int)(height/scale), title),
      m_width((int)(width/scale)), m_height((int)(height/scale)),
      m_orig_width(width), m_orig_height(height), m_scale(scale),
      m_mode(mode), m_mono(mode==Mono8)
    {
      if (m_mono) // in fullscale we will display mono image
        mp_img = new unsigned char[m_width*m_height];
      else // we will display color image
        mp_img = new unsigned char[m_width*m_height*3];
      end();
    }
    
    /** Constructor */
    DisplayWindow(int width, int height, int scale, ColorMode mode, const char* title, int posx, int posy)
    : Fl_Window(posx, posy, (int)(width/scale), (int)(height/scale), title),
      m_width((int)(width/scale)), m_height((int)(height/scale)),
      m_orig_width(width), m_orig_height(height), m_scale(scale),
      m_mode(mode), m_mono(mode==Mono8)
    {
      if (m_mono) // in fullscale we will display mono image
        mp_img = new unsigned char[m_width*m_height];
      else // we will display color image
        mp_img = new unsigned char[m_width*m_height*3];
      end();
    }
    
    /** Destructor */
    ~DisplayWindow() 
    { 
      delete[] mp_img; 
    }
    
    /** Provide the frame with the image data that should be displayed. */
    void provideFrame(unsigned char* data)
    {
      if (m_scale == 1) {
      	switch(m_mode) {
      	case Mono8: memcpy(mp_img, data, m_width*m_height); break;
      	case Rgb8: memcpy(mp_img, data, m_width*m_height*3); break;
      	case Bayer8: {
      		// "inplace" bayer
      		int w = m_width / 2;
      		int h = m_height / 2;
      		int offset_r = 0;
      		int offset_g1 = 1;
      		int offset_g2 = 2*w;
      		int offset_b = offset_g2 + 1;
      		int p1 = 3;
      		int p2 = 3*m_width;
      		int p3 = p2 + 3;
      		for(int y=0; y<h; y++) {
      			for(int x=0; x<w; x++) {
      				unsigned char* src = data + w*2*y + 2*x;
      				unsigned char r = *(src + offset_r);
      				unsigned char g = ((unsigned int)(*(src + offset_g1)) + (unsigned int)(*(src + offset_g2)))/2;
      				unsigned char b = *(src + offset_b);
      				// write 4 pixel in the result buffer
      				unsigned char* p = mp_img + 3*(m_width*2*y + 2*x);
      				p[0] = r;
      				p[1] = g;
      				p[2] = b;
      				p[p1+0] = r;
      				p[p1+1] = g;
      				p[p1+2] = b;
      				p[p2+0] = r;
      				p[p2+1] = g;
      				p[p2+2] = b;
      				p[p3+0] = r;
      				p[p3+1] = g;
      				p[p3+2] = b;
      			}
      		}
      	} break;
      	default: std::cerr << "Mode not supported for display" << std::endl;
      	};
      } else {
      	std::cerr << "Scaling not supported for display" << std::endl;
     }
      /*else 
      {
        // RGGB pattern expected
        int rpos = 0;
        int gpos = 1;
        int gpos2 = m_orig_width;
        int bpos = m_orig_width + 1;
        int dstpos = 0;
        int fact = m_orig_width * m_scale;
        for (int y=0; y<m_height; y++)
        {
          rpos = y * fact;
          gpos = rpos + 1;
          gpos2 = rpos + m_orig_width;
          bpos = gpos + m_orig_width;
          for (int x=0; x<m_width; x++)
          {
            int r = data[rpos];
            int g = (data[gpos] + data[gpos2]) >> 1; // mean of the two G pixels
            int b = data[bpos];
            mp_img[dstpos++] = r;
            mp_img[dstpos++] = g;
            mp_img[dstpos++] = b;
            rpos += m_scale;
            gpos += m_scale;
            gpos2 += m_scale;
            bpos += m_scale;
          }
        }
      }*/
      // else // do nearest neighbor scaling (code not optimized!)
      // {
      //   int pos = 0;
      //   for (int y=0; y<m_height; y++)
      //   {
      //     int y_base = y * m_scale * m_orig_width;
      //     for (int x=0; x<m_width; x++, pos++)
      //     {
      //       int x_src = x * m_scale;
      //       mp_img[pos] = data[y_base+x_src];
      //     }
      //   }
      // }
    }
    
    /** Inherited drawing method. Called whenever redraw is needed. */
    void draw()
    {
      if(m_mono)
        fl_draw_image_mono(mp_img, 0, 0, m_width, m_height);
      else
        fl_draw_image(mp_img, 0, 0, m_width, m_height);
    }
    
    /** Inherited method for intercepting events. */
    int handle(int event)
    {
      if (event == FL_RELEASE || event == FL_KEYUP)
        m_event_count++;
    }
    
    /** Method showing that we want to quit. */
    static int getEventCount()
    {
      return m_event_count;
    }
};



#endif

