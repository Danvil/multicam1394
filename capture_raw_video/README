Description:
   Program for capturing video data in the kitchen scenario at IAS. It can be
   used only for display, but also for recording by writing the camera data
   through named pipes into an encoding program like mencoder. The video data
   from multiple cameras is synchronized via external triggering (this is 
   hardcoded into the program), and corresponding timestamps are written to a 
   log file. The script 'record_videos.sh' is provided for convenience.
   If you want to set special camera parameters like whitebalance, use
   'coriander' to do the setup and capture using this program afterwards.
   If the program is not in display mode, capturing starts immediatly. If the
   program is in display mode, a press inside one of the camera windows starts
   the capturing, and the next press stops it and exits. An estimate of the
   framerate is printed 5 seconds after the program was started.
   
Usage:
   Call 'capture_raw_video' for a list of command line parameters. If you don't
   specify a prefix for the named pipes, the program will only run in display
   mode. The script 'record_videos.sh' is provided for convenience.

Packet Sizes:
   The frame rate in format7 mode is dependant on the packet sizes and the 
   camera shutter. Padding is not a problem for this program and needs not be
   treated differently. Note that the packet size must be a multiple of 4.
   Here are some values for the packetsizes given a shutter of 1600:
   15 fps theoretical:  852   practical:  868
   25 fps theoretical: 1448   practical: 1460
   30 fps theoretical: 1700   practical: 1756
   The formula for calculation is the following (for our AVT Guppys and a 400MB/s
   1394a bus):
   num_packets = (int)( 1 / (0.000125*fps) + 0.5 )
   size_packets = (780*582*8 + num_packets*8 - 1) / (num_packets*8)
                = (3631680 + num_packets*8 - 1) / (num_packets*8)
   
Libraries needed:
   libdc1394 v2
   fltk v1.1
   
Performance:
   On atradig117, capturing of 5 AVT Guppy cameras (780x582) at 25fps is
   working fine. At 30fps, only four cameras can be captured simultaneously.
   
Todo (if someone wants to invest some time):
   Setting different format 7 parameters seems to slow down everything a lot.
   This needs to be investigated. For now, I recommend always capturing at full
   format 7 resolution.
   Currently, images at full resolution are displayed in mono, all others in 
   color using downsampling. Another algorithm for color display of the large
   size images could be implemented (or even taken from libdc1394 I guess).
