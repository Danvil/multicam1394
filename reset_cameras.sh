#!/bin/sh

sudo rm /tmp/ias_video_pipe*

sudo modprobe -r video1394
sudo modprobe -r ohci1394
sudo modprobe -r raw1394
sudo modprobe -r sbp2
sudo modprobe -r eth1394
sudo modprobe -r ieee1394

sudo modprobe ieee1394
sudo modprobe video1394
sudo modprobe ohci1394
sudo modprobe raw1394
sudo modprobe sbp2

# see https://help.ubuntu.com/community/Firewire
sudo chmod 666 /dev/raw1394

